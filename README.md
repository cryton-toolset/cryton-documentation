# PROJECT HAS BEEN MOVED
The project has been moved to https://gitlab.ics.muni.cz/cryton/cryton. For more information check the [documentation](https://cryton.gitlab-pages.ics.muni.cz/).

[[_TOC_]]

# Cryton documentation

## Description
This project provides documentation for the Cryton toolset.

[Link to the documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/).

## Development
To install Cryton Documentation for development, you must install [Poetry](https://python-poetry.org/docs/).

Clone the repository:
```shell
git clone https://gitlab.ics.muni.cz/cryton/cryton-documentation.git
```

Then go to the correct directory and install the project:
```shell
cd cryton-documentation
poetry install
```

To spawn a shell use:
```shell
poetry shell
```

## Usage
To build the documentation, run:
```
mkdocs build
```

To serve the documentation, run:
```
mkdocs serve -a address:port
```

Documentation will be available on defined address and port. For example `mkdocs serve -a localhost:8001` will be 
served at [http://localhost:8001](http://localhost:8001).
