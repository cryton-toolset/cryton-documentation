Here is a curated list of modules. Please see each module's readme for more information.

## mod_cmd
A module that allows local and remote command execution.

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_cmd/README.md){target="_blank"}.

## mod_script
A module that gives you the option to execute a script (Python, sh, bash, etc.).

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_script/README.md){target="_blank"}.

## mod_medusa
A module for password brute-forcing using the [Medusa](https://github.com/jmk-foofus/medusa) tool.

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_medusa/README.md){target="_blank"}.

## mod_msf
A module that uses the [Metasploit Framework](https://www.metasploit.com/) and automizes it.

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_msf/README.md){target="_blank"}.

## mod_nmap
A module for scanning using the [nmap](https://nmap.org/) tool.

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_nmap/README.md){target="_blank"}.

## mod_wpscan
A module for scanning WordPress pages using the [WPScan](https://wpscan.com/) tool.

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_wpscan/README.md){target="_blank"}.

## mod_ffuf
[:octicons-tag-24: Modules 1.0.1]({{{ releases.modules }}}1.0.1){target="_blank"}

A module for web fuzzing using the [ffuf](https://github.com/ffuf/ffuf) tool.

More information can be found [here](https://gitlab.ics.muni.cz/cryton/cryton-modules/-/blob/master/modules/mod_ffuf/README.md){target="_blank"}.
